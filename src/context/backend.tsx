import React from 'react';

import { Backend } from '../../backend';

const backend = {
	getMushroom: async (name: string) => {
		return await Backend.getMushroom(name);
	},
	getMushrooms: async () => {
		return await Backend.getMushrooms();
	},
	getMushroomImages: async (name: string) => {
		return await Backend.getMushroomImages(name);
	}
}

export const BackendContext = React.createContext(backend)
