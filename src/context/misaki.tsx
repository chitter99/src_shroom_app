import React from 'react';
import * as ImageManipulator from 'expo-image-manipulator';

import { Backend } from '../../backend';

async function getImageSize(uri) {
	let image = await ImageManipulator.manipulateAsync(uri, []);
	return { width: image.width, height: image.height }
};

const misaki = {
	predict: async (uri: string) => {
		return await Backend.predict(uri);
	},
	predictWithCrop: async (uri: string) => {
		// we have to use getSize from expo libary.
		// because getSize in React libary sometimes returns the size in dp not px.
		let size = await getImageSize(uri);

		let bw = size.width * 0.8;
		let crop = {
			originX: (size.width - bw) / 2,
			originY: (size.height - bw) / 2,
			width: bw,
			height: bw
		};

		let image;
		try {
			image = await ImageManipulator.manipulateAsync(uri, [{ crop: crop }, { resize: { height: 100, width: 100 } }], { format: ImageManipulator.SaveFormat.JPEG });
		} catch(ex) {
			throw ex;
		}
		
		return Backend.predict(image.uri);
	}
}

export const MisakiContext = React.createContext(misaki)
