import React, { useEffect, useState, createContext, useContext } from 'react';
import { StyleSheet } from 'react-native';
import { Container, Button, Text, Icon, Spinner, Header, Content, Left } from 'native-base';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	text: {
		fontSize: 18,
		marginBottom: 20
	},
	icon: {
		fontSize: 80,
		marginBottom: 20
	}
});

interface ILoadingContext {
	loadingComponentFinished(): void;
	loadingComponentFailed(reason: string): void;
	loadingRestart(): void;
	loadingCancel(): void;
}

/**
 * Default call for LoadingContext. All methodes throw exception.
 */
class LoadingContextDefault implements ILoadingContext {
	loadingComponentFinished(): void {
		throw new Error('Method not implemented.');
	}
	loadingComponentFailed(reason: string): void {
		throw new Error('Method not implemented.');
	}
	loadingRestart(): void {
		throw new Error('Method not implemented.');
	}
	loadingCancel(): void {
		throw new Error('Method not implemented.');
	}

}

const LoadingContext = createContext(new LoadingContextDefault());

type ILoadingProps = {
	children: Array<any> | any
	onSuccess: () => void 
	onCancelling: () => void
};

interface ILoadingFailed {
	failed: boolean
	reason?: string
}

export function Loading({ children, onSuccess, onCancelling }: ILoadingProps) {
	/**
	 * Component should:
	 * - assign loading props to currently loading child
	 * - show loadingError prop when loadingComponentFailed is called
	 * - restart loading when loadingRestart is called
	 * - call onCancelling when loading is canceled
	 * - call onSuccess when loading is done
	 */

	const [ resetFlag, setResetFlag ] = useState({});
	const [ currentLoadIndex, setCurrentLoadIndex ] = useState(null);
	const [ loadingFailed, setLoadingFailed ] = useState({
		failed: false,
		reason: null
	} as ILoadingFailed);

	const context: ILoadingContext = {
		/**
		 * Currently loading component has finished loading.
		 * Continue with the next component or call onSuccess callback.
		 */
		loadingComponentFinished: () => {
			let nextLoadIndex = currentLoadIndex + 1;
			if(nextLoadIndex >= children.length) {
				onSuccess();
				setCurrentLoadIndex(null);
			} else {
				setCurrentLoadIndex(nextLoadIndex);
			}
		},
		loadingComponentFailed: (reason) => {
			setLoadingFailed({
				failed: true,
				reason: reason
			});
		},
		loadingCancel: () => onCancelling,
		loadingRestart: () => {
			setResetFlag({});
		}
	};

	const loadingTemplate = (children, showHeader=false) => {
		return <LoadingContext.Provider value={ context }>
			<Container>
				{ showHeader ? <Header>
					<Left>
						<Button transparent onPress={ () => onCancelling() }>
							<Icon name='arrow-back' />
						</Button>
					</Left>
				</Header> : null }
				<Content contentContainerStyle={ styles.container }>
					{ children }
				</Content>
			</Container>
		</LoadingContext.Provider>;
	};

	/**
	 * Start loading prozedure
	 */
	useEffect(() => {
		setLoadingFailed({
			failed: false,
			reason: null
		});
		setCurrentLoadIndex(0);
	}, [ resetFlag ]);

	if(loadingFailed.failed == true) {
		return loadingTemplate(<LoadingError text={ loadingFailed.reason } />, true);
	}

	if(currentLoadIndex != null) {
		let index = 0;
		let newChildren = React.Children.map(children, (child) => {
			let clone = React.cloneElement(child, {
				loading: currentLoadIndex == index
			});
			index++;
			return clone;
		});

		return loadingTemplate(newChildren);
	}

	return <Text>Done loading...</Text>;
}

interface ILoadableComponentProps {
	loading?: boolean
}

interface ILoadableComponentBaseProps extends ILoadableComponentProps {
	children?: Array<any> | any
	onStartLoad(): void;
}

/**
 * Component which detects if he should be loading or not. Renders child components when he is loading.
 * @param props props for component
 */
function LoadableComponent({ loading = false, children, onStartLoad }: ILoadableComponentBaseProps) {
	useEffect(() => {
		if(loading == true) {
			onStartLoad();
		}
	}, [loading]);

	if(!loading) {
		// We don't want to render anything
		return null;
	}
	return children;
}

interface ILoadStateAsyncProps extends ILoadableComponentProps {
	text?: string
	errorText?: string
	onLoading(): Promise<void>;
}

export function LoadStateAsync({ loading, text = 'Loading...', errorText, onLoading }: ILoadStateAsyncProps) {
	const loadingContext = useContext(LoadingContext);
	const onStartLoad = () => {
		async function loadStateAsync() {
			try {
				await onLoading();
				// Make sure loadingComponentFinished is only called if loading was successful
				loadingContext.loadingComponentFinished();
			} catch(ex) {
				loadingContext.loadingComponentFailed(errorText || (ex as Error).message);
			}
		}
		loadStateAsync();
	};

	// We have to forward all props from the LoadableComponentProps interface
	return <LoadableComponent loading={ loading } onStartLoad={ onStartLoad }>
		<Spinner color='blue' />
		<Text style={ styles.text }>{ text }</Text>
	</LoadableComponent>;
}

type ILoadingErrorProps = {
	text?: string
};

function LoadingError({ text = 'Something went wrong!' }: ILoadingErrorProps) {
	const loadingContext = useContext(LoadingContext);
	
	return <>
		<Icon style={[ styles.icon, {  color: 'red' } ]} type='MaterialIcons' name='error-outline' />
		<Text style={ styles.text }>{ text }</Text>
		<Button bordered danger iconRight onPress={ loadingContext.loadingRestart }>
			<Text>Neu versuchen</Text>
			<Icon type='AntDesign' name='reload1' />
		</Button>
	</>
}
