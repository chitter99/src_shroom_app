import React, { useState, useEffect } from 'react';
import { TouchableOpacity, Dimensions, StyleSheet } from 'react-native';	
import { View, Container, Text } from 'native-base';
import { Camera } from 'expo-camera';

import Frame from '../../../assets/camera/frame.svg';
import Shutter from '../../../assets/camera/shutter.svg';

const styles = StyleSheet.create({
	camera: {
		flex: 1,
		justifyContent: 'flex-start'
	},
	viewText: {
		flex: 1,
		alignItems: 'center', 
		justifyContent: 'center'
	},
	viewMask: {
		flex: 4,
		alignItems: 'center', 
		justifyContent: 'center'
	},
	viewMaskBox: {
		borderWidth: 2,
		borderColor: 'white'
	},
	viewShutter: {
		flex: 1,
		alignItems: 'center', 
		justifyContent: 'center'
	},
	text: {
		fontSize: 24,
		textAlign: 'center',
		color: 'white'
	}
});

export function CameraSceen({ navigation }) {
	const [hasPermission, setHasPermission] = useState(null);

	useEffect(() => {
		(async () => {
			const { status } = await Camera.requestPermissionsAsync();
			setHasPermission(status === 'granted');
		})();
	});

	if (hasPermission === null) {
		return <View />;
	}
	if (hasPermission === false) {
		return <Text>No access to camera</Text>;
	}

	let takePhoto = async () => {
		let photo = await this.camera.takePictureAsync();
		navigation.navigate('Preview', { uri: photo.uri });
	};

	const { width } = Dimensions.get('window');

	return <Container> 
		<Camera 
			ref={ (ref) => { this.camera = ref; }}
			type='back'
			autoFocus='on'
			ratio='16:9'
			focusDepth={ 1 }
			style={ styles.camera } >
			<TouchableOpacity
				style={{ flex: 1 }}
				onPress={ async () => takePhoto() }>
				<View style={ styles.viewText }>
					<Text style={ styles.text }>Pilz ins Quadrat</Text>
				</View>
				<View style={ styles.viewMask }>
					<View style={[ styles.viewMaskBox, { width: width * 0.8, height: width * 0.8 } ]}></View>
				</View>
				<View style={ styles.viewShutter }>
					<Shutter height={50} width={50} />
				</View>
			</TouchableOpacity>
		</Camera>
	</Container>;
}
