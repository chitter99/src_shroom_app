import React, { useState, useEffect, useContext } from 'react';

import { MisakiContext } from '../../context/misaki';
import { BackendContext } from '../../context/backend';
import { Loading, LoadStateAsync } from '../../components/loading';


export function UploadingScreen({ route, navigation }) {
	const [ predictions, setPredictions ] = useState(null);
	const [ matches, setMatches ] = useState(null);
	const [ finished, setFinished ] = useState(false);

	const misaki = useContext(MisakiContext);
	const backend = useContext(BackendContext);

	const { uri } = route.params;

	useEffect(() => {
		if(finished == true) {
			navigation.navigate('Prediction', { matches: matches });
		}
	}, [ finished ]);

	async function loadPrediction() {
		let result = await misaki.predictWithCrop(uri);
		setPredictions(result.data.data.prediction);
	}
	
	async function loadMatches() {
		let result = await backend.getMushrooms();
		
		let m = result.data.data.mushrooms.map((m) => {
			let prediction = predictions.find((p) => p.uid == m.name);
			if (Math.round(prediction.value * 100) == 0) return null;
			return {
				mushroom: m,
				prediction: prediction
			};
		});
		m = m.filter((e) => e != null);
		m = m.sort((p1, p2) => {
			if(p1.prediction.value > p2.prediction.value) {
				return -1;
			}
			if(p1.prediction.value < p2.prediction.value) {
				return 1;
			}
			return 0;
		});

		setMatches(m);
	}

	return <Loading onSuccess={ () => setFinished(true) } onCancelling={ () => navigation.navigate('Camera') }>
		<LoadStateAsync text='Pilz wird hochgeladen und analysiert' errorText='Pilz konnte nicht hochgeladen werden' onLoading={ () => loadPrediction() } />
		<LoadStateAsync text='Treffer werden vorbereitet' errorText='Treffer konnten nicht vorbereitet werden' onLoading={ () => loadMatches() } />
	</Loading>;
}
