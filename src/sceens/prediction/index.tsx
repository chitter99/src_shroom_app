import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { Container, Button, Icon, Content, Left, Title, Body, Header, Card, CardItem, Right, Text, View } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		padding: 20
	},
	viewNotFound: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	textNotFound: {
		fontSize: 16
	},
	viewTitle: {
		flexDirection: 'row'
	},
	iconTitle: {
		fontSize: 24,
		paddingEnd: 10
	},
	textTitle: {
		fontSize: 18,
		paddingBottom: 18
	},
	iconNotFound: {
		fontSize: 80,
		marginBottom: 20
	},
	predictionCardMatchIcon: {
		fontSize: 16
	},
	predictionCardMatchText: {
		fontSize: 14
	},
	predictionCardMatchMetaIcon: {
		fontSize: 16,
		paddingEnd: 10
	},
	predictionCardMatchMetaText: {
		fontSize: 14
	},
	predictionCardMatchMetaView: {
		flexDirection: 'row'
	}
});

export function PredictionScreen({ route, navigation }) {
	const { matches } = route.params;

	return <Container>
		<Header>
			<Left>
				<Button transparent onPress={ () => navigation.navigate('Camera') }>
					<Icon name='arrow-back' />
				</Button>
			</Left>
			<Body>
				<Title>Misaki denkt...</Title>
			</Body>
		</Header>
		<Content scrollEnabled={ false } contentContainerStyle={ styles.container }>
			<Prediction matches={ matches }></Prediction>
		</Content>
	</Container>;
}

function PredictionCard({ mushroom, prediction }) {
	let match = Math.round(prediction.value * 100);
	return <Card>
		<CardItem header>
			<Text>{ mushroom.title }</Text>
		</CardItem>
		<CardItem>
			<Image source={{ uri: mushroom.thumbnailUrl }} style={{height: 200, width: null, flex: 1}} />
		</CardItem>
		<CardItem>
			<Left>
				<Icon style={ styles.predictionCardMatchIcon } type='Entypo' name='magnifying-glass' />
				<Text style={ styles.predictionCardMatchText }>{ `${match}% Übereinstimmung` }</Text>
			</Left>
			<Right>
				{ mushroom.edible ? <PredictionCardMeta type={ PredictionCardMetaType.Edible } /> : null }
				{ mushroom.poisonous ? <PredictionCardMeta type={ PredictionCardMetaType.Poisonous } /> : null }
			</Right>
		</CardItem>
	</Card>;
}

enum PredictionCardMetaType {
	Edible,
	Poisonous
}

type PredictionCardMetaProps = {
	type: PredictionCardMetaType
}

function PredictionCardMeta({ type }: PredictionCardMetaProps) {
	let [ color, itype, name, text ] = ['', '', '', ''];
	if(type == PredictionCardMetaType.Edible) {
		color = 'green';
		itype = 'MaterialCommunityIcons';
		name = 'food';
		text = 'Essbar';
	} else {
		color = 'red';
		itype = 'FontAwesome';
		name = 'warning';
		text = 'Giftig';
	}
	return <View style={ styles.predictionCardMatchMetaView }>
		<Icon style={[ styles.predictionCardMatchMetaIcon, { color: color } ]} type={ itype } name={ name } />
		<Text style={ styles.predictionCardMatchMetaText }>{ text }</Text>
	</View>;
}


function Prediction({ matches }) {
	if(matches.length == 0) {
		return <View style={ styles.viewNotFound }>
			<Icon style={ styles.iconNotFound } name='frowno' type='AntDesign' />
			<Text style={ styles.textNotFound }>Leider konnte ich keinen Pilz wiedererkennen</Text>
		</View>;
	}

	let text = '';
	if(matches.length > 1) {
		text = 'das der Pilz einen der folgenden ist';
	} else {
		text = 'der Pilz im Bild folgender ist';
	}

	return <>
		<View style={ styles.viewTitle }>
			<Icon style={ styles.iconTitle } active name='smileo' type='AntDesign' />
			<Text style={ styles.textTitle }>{ text }</Text>
		</View>
		<ScrollView>
			{ matches.map((m) => <PredictionCard key={ m.mushroom.name } mushroom={ m.mushroom } prediction={ m.prediction } ></PredictionCard>) }
		</ScrollView>
	</>;
}

