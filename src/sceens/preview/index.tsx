import React from 'react';
import { StyleSheet, ImageBackground, TouchableOpacity, Dimensions } from 'react-native';
import { View, Container, Icon, Text } from 'native-base';

import Frame from '../../../assets/camera/frame_white.svg';

const styles = StyleSheet.create({
	preview: {
		width: '100%',
		height: '100%'
	},
	icon: {
		fontSize: 80,
		padding: 40,
		color: 'white'
	},
	viewText: {
		flex: 1,
		alignItems: 'center', 
		justifyContent: 'center'
	},
	viewMask: {
		flex: 4,
		alignItems: 'center', 
		justifyContent: 'center'
	},
	viewMaskBox: {
		borderWidth: 2,
		borderColor: 'white'
	},
	actionsView: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'flex-end'
	},
	text: {
		fontSize: 24,
		textAlign: 'center',
		color: 'white'
	}
});

export function PreviewScreen({ route, navigation }) {
	let { uri } = route.params;
	const { width } = Dimensions.get('window');

	return <Container>
		<ImageBackground style={ styles.preview } source={{ uri: uri }}>
			<View style={ styles.viewText }>
				<Text style={ styles.text }>Gut so?</Text>
			</View>
			<View style={ styles.viewMask }>
				<View style={ styles.viewMask }>
					<View style={[ styles.viewMaskBox, { width: width * 0.8, height: width * 0.8 } ]}></View>
				</View>
			</View>
			<View style={ styles.actionsView }>
				<TouchableOpacity onPress={ () => { navigation.goBack() } }>
					<Icon style={[ styles.icon, { color: 'red' } ]} type='AntDesign' name='close' />
				</TouchableOpacity>
				<TouchableOpacity onPress={ () => { navigation.navigate('Uploading', { uri: uri }) } }>
					<Icon style={[ styles.icon, { color: 'green' } ]} type='AntDesign' name='check' />
				</TouchableOpacity>
			</View>
		</ImageBackground>
    </Container>;
};
