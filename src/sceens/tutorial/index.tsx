import React from 'react';
import { StyleSheet } from 'react-native';
import { View, Container, Button, Text, Icon, Header, Body, Title, Content, Footer, Card, CardItem } from 'native-base';

const styles = StyleSheet.create({
	buttonView: {
		flex: 1,
		justifyContent: 'flex-end',
		padding: 30
	}
});

export function TutorialScreen({ navigation }) {
    return <Container style={{ flex: 1 }}>
		<Header>
			<Body>
				<Title>What Mushroom?</Title>
			</Body>
		</Header>
		<Content>
			<Card transparent>
				<CardItem header>
					<Icon name='camera'></Icon>
					<Text>Pilz fotografieren</Text>
				</CardItem>
				<CardItem>
					<Body>
						<Text>Egal ob unterwegs im Wald oder im eigenen Garten. Achten Sie darauf, dass sich der Pilz komplett im angegeben Quadrat befindet.</Text>
					</Body>
				</CardItem>
			</Card>
			<Card transparent>
				<CardItem header>
					<Icon type='AntDesign' name='upload'></Icon>
					<Text>Pilz hochladen</Text>
				</CardItem>
				<CardItem>
					<Body>
						<Text>Tippen Sie auf den Bildschirm oder den runden Knopf. Um das Foto klassifizieren zu lassen, bestätigen Sie dies mit dem grünen Hacken. Um das aktuelle Bild zu verwerfen und ein neues aufzunehmen, drücken Sie auf das rote Kreuz.</Text>
					</Body>
				</CardItem>
			</Card>
			<Card transparent>
				<CardItem header>
					<Icon type='AntDesign' name='tool'></Icon>
					<Text>Pilz analyse mit Misaki</Text>
				</CardItem>
				<CardItem>
					<Body>
						<Text>Die Mushroom Identification Super Artificial K... Intelligence wird dann den aufgenohmenen Pilz mit den 15 für Ihn bekannten Pilz-Arten vergleichen und die wahrscheinlichsten Arten in der App anzeigen.</Text>
					</Body>
				</CardItem>
			</Card>
			<Card transparent>
				<CardItem header>
					<Icon type='FontAwesome' name='warning'></Icon>
					<Text>Verlassen Sie sich nicht auf Misaki!</Text>
				</CardItem>
				<CardItem>
					<Body>
						<Text>Die App gibt Ihnen zwar an ob die jeweiligen Pilz-Arten essbar sind oder nicht. Jedoch können Sie sich nicht auf die Klassifizierung verlassen und weder ich noch Misaki übernimmt Haftung auf die Angaben. Bitte suchen Sie, IMMER vor dem verspeisen eines gesammelten Pilzes Ihre lokale Pilz-Tester auf.</Text>
					</Body>
				</CardItem>
			</Card>
			<View style={ styles.buttonView }>
				<Button bordered primary iconRight onPress={() => { navigation.navigate('Camera') }}>
					<Text>Ok, Weiter</Text>
					<Icon name='arrow-forward' />
				</Button>
			</View>
		</Content>
    </Container>;
};
