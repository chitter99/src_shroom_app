import React from 'react';
import { StatusBar, View } from 'react-native';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import { TutorialScreen } from './src/sceens/tutorial';
import { CameraSceen } from './src/sceens/camera';
import { PreviewScreen } from './src/sceens/preview';
import { UploadingScreen } from './src/sceens/uploading';
import { PredictionScreen } from './src/sceens/prediction';

const Stack = createStackNavigator();

export default class App extends React.Component<any, any> {
	constructor(props) {
		super(props);
		this.state = {
			isReady: false
		};
	}

	async componentDidMount() {
		await Font.loadAsync({
			Roboto: require('native-base/Fonts/Roboto.ttf'),
			Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
			...Ionicons.font,
		});
		this.setState({ isReady: true });
	}

	render() {
		if(!this.state.isReady) return <AppLoading></AppLoading>;
		return <View style={{ flex: 1 }}>
			<NavigationContainer>
				<Stack.Navigator headerMode='none'>
					<Stack.Screen name='Tutorial' component={ TutorialScreen } />
					<Stack.Screen name='Camera' component={ CameraSceen } />
					<Stack.Screen name='Preview' component={ PreviewScreen } />
					<Stack.Screen name='Uploading' component={ UploadingScreen } />
					<Stack.Screen name='Prediction' component={ PredictionScreen } />
				</Stack.Navigator>
			</NavigationContainer>
		</View>;
	}
};