import axios from 'axios';

import { Prediction, Mushroom, MushroomImage } from './model';
import { get_file_mme } from './utils';

const API = 'https://whatmushroom.app/api';

export enum ResponseStatus {
	Ok = 'ok',
	Error = 'error'
}

enum ResponseErrorReason {
	MissingPhoto = 'missing_image',
	PredictionFailed = 'prediction_failed'
}

interface ResponseDataBase<D> {
	status: ResponseStatus
	reason?: ResponseErrorReason
	data?: D
}

class ResponsePredictionData {
	prediction: Array<Prediction>
}

class ResponseMushroomsData {
    mushrooms: Array<Mushroom>
}

class ResponseMushroomData {
    mushroom: Mushroom
}

class ResponseMushroomImagesData {
    images: Array<MushroomImage>
}

export class Backend {
    /**
     * Uploads local file to misaki and returns prediction
     * @param localuri localuri to file
     */
	static async predict(localuri) {
        let filename = localuri.split('/').pop();
        let type = get_file_mme(filename, 'image');
		
		let data = new FormData();
		data.append('image', { uri: localuri, name: filename, type });

		return await axios.put<ResponseDataBase<ResponsePredictionData>>(API + '/model/misaki/predict', data, {
			headers: {
				'Content-Type': 'multipart/form-data'
			}
		});
    }
    
    /**
     * Gets all known mushrooms from server
     */
    static async getMushrooms() {
        return await axios.get<ResponseDataBase<ResponseMushroomsData>>(API + '/mushrooms');
    }

    /**
     * Gets details from the specific mushroom from the server
     * @param name name of mushroom 
     */
    static async getMushroom(name: string) {
        return await axios.get<ResponseDataBase<ResponseMushroomData>>(API + `/mushroom/${name}`);
    }

    /**
     * Gets all images from the specific mushrooms
     * @param name name of mushroom 
     */
    static async getMushroomImages(name: string) {
        return await axios.get<ResponseDataBase<ResponseMushroomImagesData>>(API + `/mushroom/${name}/images`);
    }
}
