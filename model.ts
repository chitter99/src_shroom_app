
export class Prediction {
    uid: string
    nb_class: number
    value: number
}
  
export class Mushroom {
    name: string
    title: string
    description: string
    poisonous: boolean
    edible: boolean
    thumbnailUrl: string
}

export class MushroomImage {
    url: string
    mushroom: string
    order: number
}
