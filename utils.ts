
/**
 * Gets the mme type of the file
 * @param filename name of file
 * @param type mme suffix (e.g. image or application)
 */
export function get_file_mme(filename, type) {
	// execute regex to get extenstion
	let match = /\.(\w+)$/.exec(filename);
	return match ? `${type}/${match[1]}` : type;
}
